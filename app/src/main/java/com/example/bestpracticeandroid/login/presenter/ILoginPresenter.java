package com.example.bestpracticeandroid.login.presenter;

public interface ILoginPresenter {
    boolean onLogin(String username, String password);
}
