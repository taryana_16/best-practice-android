package com.example.bestpracticeandroid.login.view.impl;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bestpracticeandroid.R;
import com.example.bestpracticeandroid.login.presenter.impl.LoginPresenter;
import com.example.bestpracticeandroid.login.view.LoginView;
import com.example.bestpracticeandroid.welcome.view.impl.MovieActivity;

import es.dmoral.toasty.Toasty;

public class LoginActivity extends AppCompatActivity implements LoginView {

    EditText edtUserName, edtPassword;
    Button btnLogin;

    LoginPresenter loginPresenter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtUserName = findViewById(R.id.edtUserName);
        edtPassword = findViewById(R.id.edtPassword);
        btnLogin = findViewById(R.id.btnLogin);

        loginPresenter = new LoginPresenter(this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = edtUserName.getText().toString();
                String password = edtPassword.getText().toString();
                loginPresenter.onLogin(username, password);
                if(loginPresenter.onLogin(username,password)) {
                    Intent intent = new Intent(LoginActivity.this, MovieActivity.class);
                    startActivity(intent);
                }
            }
        });
    }


    @Override
    public void showErrorUserName(String username) {
        Toasty.error(this, username, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorPassword(String password) {
        Toasty.error(this, password, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String error) {
        Toasty.error(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showSuccess(String success) {
        Toasty.success(this, success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {

    }
}
