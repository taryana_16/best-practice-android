package com.example.bestpracticeandroid.login.view;

public interface LoginView {
    void showErrorUserName(String username);
    void showErrorPassword(String password);
    void showError(String error);
    void showSuccess(String success);
    void showProgress();
}
