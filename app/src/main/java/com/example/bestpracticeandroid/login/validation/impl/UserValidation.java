package com.example.bestpracticeandroid.login.validation.impl;

import android.text.TextUtils;

import com.example.bestpracticeandroid.login.model.User;
import com.example.bestpracticeandroid.login.validation.IUserValidation;

public class UserValidation implements IUserValidation {

    String username, password;

    public UserValidation(String username, String password) {
        this.username = username;
        this.password = password;
    }


    @Override
    public int isValidData() {

        User user = new User(username, password);

        if (TextUtils.isEmpty(user.getUsername())) {
            return 0;
        } else if (TextUtils.isEmpty(user.getPassword())) {
            return 1;
        } else if (user.getUsername().length() < 6) {
            return 2;
        } else if (user.getPassword().length() < 6) {
            return 3;
        } else if (!user.getUsername().equals("taryana") || !user.getPassword().equals("taryana")) {
            return 4;
        } else {
            return -1;
        }
    }
}
