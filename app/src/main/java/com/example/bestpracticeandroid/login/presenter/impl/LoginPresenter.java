package com.example.bestpracticeandroid.login.presenter.impl;

import com.example.bestpracticeandroid.login.model.User;
import com.example.bestpracticeandroid.login.presenter.ILoginPresenter;
import com.example.bestpracticeandroid.login.validation.impl.UserValidation;
import com.example.bestpracticeandroid.login.view.LoginView;

public class LoginPresenter implements ILoginPresenter {

    LoginView loginView;

    public LoginPresenter(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public boolean onLogin(String username, String password) {

        User user = new User(username, password);
        UserValidation UserValidation = new UserValidation(username, password);
        int loginCode = UserValidation.isValidData();

        if(loginCode == 0) {
            loginView.showErrorUserName("You must enter your username");
        } else if(loginCode == 1) {
            loginView.showErrorPassword("You must enter your password");
        } else if(loginCode == 2) {
            loginView.showErrorPassword("Username length must be greater than 5");
        } else if(loginCode == 3) {
            loginView.showErrorUserName("Password length must be greater than 5");
        } else if(loginCode == 4) {
            loginView.showError("Username or Password is not valid");
        }
        else {
            loginView.showSuccess("Login success");
            return true;
        }
        return false;
    }
}
