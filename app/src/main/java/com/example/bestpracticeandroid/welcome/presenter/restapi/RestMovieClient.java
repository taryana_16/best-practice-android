package com.example.bestpracticeandroid.welcome.presenter.restapi;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestMovieClient {

    static String Base_URL = "https://api.themoviedb.org/3/";

//    550?api_key=ffa9d2d2be71d38abfb944b1867722f3

    static OkHttpClient.Builder httpCliBuilder = new OkHttpClient.Builder();
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        Retrofit.Builder retBuilder = new Retrofit.Builder().baseUrl(Base_URL)
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retro = retBuilder.client(httpCliBuilder.build()).build();
        if (retrofit == null) {
            retrofit = retro;
        }
        return retrofit;
    }
}
