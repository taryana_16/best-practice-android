package com.example.bestpracticeandroid.welcome.view;


public interface IMovieView {
    void showError(String error);
    void showSuccess(String success);
}
