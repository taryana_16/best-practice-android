package com.example.bestpracticeandroid.welcome.view.impl;


import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.bestpracticeandroid.R;
import com.example.bestpracticeandroid.welcome.model.Movie;
import com.example.bestpracticeandroid.welcome.model.response.MovieResponse;
import com.example.bestpracticeandroid.welcome.presenter.impl.MoviePresenter;
import com.example.bestpracticeandroid.welcome.presenter.restapi.ApiInterface;
import com.example.bestpracticeandroid.welcome.presenter.restapi.RestMovieClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieActivity extends AppCompatActivity  {


    private static final String TAG = MovieActivity.class.getSimpleName();
    private final static String API_KEY = "ffa9d2d2be71d38abfb944b1867722f3";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        if(API_KEY.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please obtain your API KEY first from themoviedb.org", Toast.LENGTH_LONG).show();
            return;
        }

        ApiInterface apiInterface;
        RecyclerView.Adapter adapter;
        RecyclerView.LayoutManager layoutManager;

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rvListMovie);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        apiInterface = RestMovieClient.getClient().create(ApiInterface.class);
        Call<MovieResponse> call = apiInterface.getTopRatedMovies(API_KEY);

        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                List<Movie> movies = response.body().getResults();
                Log.d(TAG, "Number of movies received: " + movies.size());
                Toast.makeText(MovieActivity.this, "Number of movies received: " + movies.size(), Toast.LENGTH_LONG).show();
                recyclerView.setAdapter(new MoviePresenter(movies, R.layout.list_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
            }
        });
    }
}
