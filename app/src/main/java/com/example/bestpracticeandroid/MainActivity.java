package com.example.bestpracticeandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.bestpracticeandroid.login.view.impl.LoginActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
